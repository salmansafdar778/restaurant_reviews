<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {

    Route::get('/', [App\Http\Controllers\UserController::class, 'index'])->name('users');
    Route::get('/all', [App\Http\Controllers\UserController::class, 'all_users'])->name('user.all');
    Route::get('/getUsers', [App\Http\Controllers\UserController::class, 'getUsers'])->name('user.getUsers');
    Route::get('/create', [App\Http\Controllers\UserController::class, 'create'])->name('user.create');
    Route::get('/edit/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('user.edit');
    Route::post('/store', [App\Http\Controllers\UserController::class, 'store'])->name('user.store');
    Route::put('/update/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('user.update');
    Route::get('/delete/{id}', [App\Http\Controllers\UserController::class, 'delete'])->name('user.delete');

});



Route::group(['prefix' => 'restaurant', 'middleware' => 'auth'], function () {
    Route::get('/', [App\Http\Controllers\RestaurantController::class, 'index'])->name('restaurants');
    Route::get('/getRestaurant', [App\Http\Controllers\RestaurantController::class, 'getRestaurant'])->name('restaurant.getRestaurant');
    Route::get('/create', [App\Http\Controllers\RestaurantController::class, 'create'])->name('restaurant.create');
    Route::get('/edit/{id}', [App\Http\Controllers\RestaurantController::class, 'edit'])->name('restaurant.edit');
    Route::get('/view/{id}', [App\Http\Controllers\RestaurantController::class, 'view'])->name('restaurant.view');
    Route::post('/store', [App\Http\Controllers\RestaurantController::class, 'store'])->name('restaurant.store');
    Route::put('/update/{id}', [App\Http\Controllers\RestaurantController::class, 'update'])->name('restaurant.update');
    Route::get('/delete/{id}', [App\Http\Controllers\RestaurantController::class, 'delete'])->name('restaurant.delete');

});

Route::group(['prefix' => 'comment', 'middleware' => 'auth'], function () {
    Route::get('/', [App\Http\Controllers\CommentController::class, 'index'])->name('comments');
    Route::get('/getComments', [App\Http\Controllers\CommentController::class, 'getComments'])->name('comment.getComments');
    Route::get('/add_reply/{id}', [App\Http\Controllers\CommentController::class, 'add_reply'])->name('comment.add_reply');
    Route::put('/save_reply/{id}', [App\Http\Controllers\CommentController::class, 'save_reply'])->name('comment.save_reply');
    Route::get('/delete/{id}', [App\Http\Controllers\CommentController::class, 'delete'])->name('comment.delete');

});


