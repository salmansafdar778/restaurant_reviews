<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/signup', [App\Http\Controllers\Api\SignupController::class, 'index']);
Route::post('/login', [App\Http\Controllers\Api\LoginController::class, 'index']);
Route::post('/logout', [App\Http\Controllers\Api\LoginController::class, 'logout'])->middleware(['auth:api']);


Route::group([ 'prefix'=>'user', 'middleware' => ['auth:api']], function () {
    Route::get('/profile', [App\Http\Controllers\Api\UserController::class, 'userProfile']);
    Route::post('/update', [App\Http\Controllers\Api\UserController::class, 'updateProfile']);
    Route::post('/updatePassword', [App\Http\Controllers\Api\UserController::class, 'updatePassword']);
});


Route::group([ 'prefix'=>'restaurant', 'middleware' => ['auth:api']], function () {
    Route::get('/all', [App\Http\Controllers\Api\RestaurantController::class, 'all']);
    Route::get('/show/{id}', [App\Http\Controllers\Api\RestaurantController::class, 'show']);
    Route::get('/getComment/{id}', [App\Http\Controllers\Api\RestaurantController::class, 'getComment']);
});

Route::group([ 'prefix'=>'comment', 'middleware' => ['auth:api']], function () {
    Route::post('/add', [App\Http\Controllers\Api\CommentController::class, 'add']);
});
