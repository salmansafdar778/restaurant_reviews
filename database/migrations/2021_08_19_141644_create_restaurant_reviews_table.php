<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant_reviews', function (Blueprint $table) {
            $table->id();
            $table->double('rating');
            $table->text('comment');
            $table->text('reply')->nullable();
            $table->unsignedBigInteger('restaurant_id');
            $table->unsignedBigInteger('comment_user_id');
            $table->unsignedBigInteger('reply_user_id')->nullable();

            $table->foreign('restaurant_id')->references('id')
                ->on('restaurants')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('comment_user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('reply_user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant_reviews');
    }
}
