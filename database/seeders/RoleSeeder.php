<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'SuperAdmin',
            'guard_name' => 'web'
        ]);

        $role = Role::create([
            'name' => 'Owner',
            'guard_name' => 'web'
        ]);

        Role::create([
            'name' => 'User',
            'guard_name' => 'web'
        ]);
    }
}
