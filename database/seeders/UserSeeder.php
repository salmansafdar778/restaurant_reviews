<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // create demo users
        $user = User::factory()->create([
            'name' => 'Super Admin',
            'email' => 'admin@test.com',
            'password' => 'secret123',
        ]);
        $user->assignRole('SuperAdmin');

        $user = User::factory()->create([
            'name' => 'Owner',
            'email' => 'owner@test.com',
            'password' => 'secret123',
        ]);
        $user->assignRole('Owner');

        $user = User::factory()->create([
            'name' => 'User',
            'email' => 'user@test.com',
            'password' => 'secret123',
        ]);
        $user->assignRole('User');


    }
}
