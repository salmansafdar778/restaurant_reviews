@extends('layouts.main')
@section('title','Dashboard')
@section('breadcrumb','Welcome to Review Restaurants,')

@section('content')

<div class="row">
<div class="col-lg-6">
<a href="#">
<div class="media flex-column flex-md-row media-bg">
    <img src="/assets/img/application.png" alt="#">
    <div class="media-body align-self-center">
        <h4>Apply for NOC</h4>
    </div>
</div>
</a>
</div>
<div class="col-lg-6">
<a href="#">
<div class="media flex-column flex-md-row media-bg">
    <img src="/assets/img/certificate.png" alt="#">
    <div class="media-body align-self-center">
        <h4>Certificates</h4>
    </div>
</div>
</a>
</div>
</div>
<div class="page-title-breadcrumb mt-3">
<div class="pull-left">
<h1 class="page-title pb-2">My Application Listings</h1>
</div>
</div>
<table id="table_id" class="display">
<thead>
<tr>
    <th>Sr.</th>
    <th>User Name</th>
    <th>Title</th>
    <th>Category</th>
    <th>Submission Date</th>
    <th>Status</th>
    <th>Action</th>
</tr>
</thead>
<tbody>


</tbody>
</table>

@endsection

@section('jsfile')

<script>
    $(function () {

    table.destroy();
    table = $('#table_id').DataTable({
        stateSave: true,
        processing: true,
        serverSide: true,
        ordering:  false,
        ajax: "{{ route('user.all') }}",
        "order": [],
        'columnDefs': [{ 'orderable': false, 'targets': 0 }],
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable: false, searchable: false},
            {data: 'user', name: 'user'},
            {data: 'title', name: 'title'},
            {data: 'category', name: 'category'},
            {data: 'submittion_date', name: 'submittion_date'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action'},
        ]
    });

  });
</script>

@endsection
