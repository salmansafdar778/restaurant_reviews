@extends('layouts.main')
@section('title','Edit User')
@section('breadcrumb','Edit User')

@section('content')

<div class="row affix-row">

    <div class="col-sm-12 col-md-12">
    <div class="affix-content">

        @livewire('users',['user'=>$user, 'isEdit'=>true,'role'=>$user->roles[0]->name ?? ''])

    </div>
    </div>
</div>


@endsection

@section('jsfile')

@endsection
