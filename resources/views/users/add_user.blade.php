@extends('layouts.main')
@section('title','Add User')
@section('breadcrumb','Add User')

@section('content')

<div class="row affix-row">

    <div class="col-sm-12 col-md-12">
    <div class="affix-content">
        @livewire('users')
    </div>
    </div>
</div>


@endsection

@section('jsfile')


@endsection
