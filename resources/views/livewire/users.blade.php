<div>

    @if(session()->has('message'))
        <div class="alert alert-danger">
            {{ session('message') }}
        </div>
    @endif

    <form wire:submit.prevent="store">
        @csrf
        <div class="row">

            <div class="col-lg-6">
                <x-form.text.text label="Full Name" type="text" id="name" name="name" value=""
                        placeHolder="Type your full name" class="" />
            </div>

            <div class="col-lg-6">
                <x-form.text.text label="Email" type="text" id="email" name="email" value=""
                                  placeHolder="Type your email" class="" />
            </div>

            <div class="col-lg-6">
                <x-form.text.text label="Password" type="password" id="password" name="password" value=""
                                  placeHolder="Type your password" class="" />
            </div>

            <div class="col-lg-6">
                <x-form.text.text label="Re type password" type="password" id="password_confirmation" name="password_confirmation" value=""
                                  placeHolder="Type your password again" class="" />
            </div>

            <div class="col-lg-6">
                <x-form.select.select label="User Role" type="select" id="role" name="role" value=""
                                  placeHolder="Select user role" class="" :records="$roles" />
            </div>

            <div class="col-lg-6">
                <x-form.text.text label="Upload Profile" type="file" id="image" name="image" value=""
                                  placeHolder="Upload your image" class="" />
            </div>


            <div class="col-lg-12 btn-submit mt-2 mb-2">
                <button type="button" wire:click="cancel()"  class="btn btn-secondary">Cancel</button>
                <button type="submit" class="btn btn-primary">{{$isEdit ? 'Update' : 'Save'}}</button>
            </div>
        </div>
    </form>
</div>

