<div>

    @if(session()->has('message'))
        <div class="alert alert-danger">
            {{ session('message') }}
        </div>
    @endif

    <form wire:submit.prevent="store">
        @csrf
        <div class="row">

            <div class="col-lg-6">
                <label class="label-font">Name</label>
                <input type="text" id="name" name="name" wire:model="name" class="form-control mb-3"
                       placeholder="Name" value="{{old('name')}}">

                @error('name')
                <span class="text-danger">{{ $errors->first('name') }}</span>
                @enderror
            </div>

            <div class="col-lg-12 btn-submit mt-2 mb-2">
                <button type="button" wire:click="cancel()"  class="btn btn-secondary">Cancel</button>
                <button type="submit" class="btn btn-primary">{{$isEdit ? 'Update' : 'Save'}}</button>
            </div>
        </div>
    </form>
</div>
