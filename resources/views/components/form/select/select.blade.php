<div>

        <label class="label-font">{{$label}}</label>

        <select name="{{$name}}" id="{{$id}}" class="form-control mb-3 {{$class}}" wire:model="{{$name}}">
            <option value="">{{$placeHolder}}</option>
            @foreach($records as $record)
                <option value="{{$record->name}}" >{{$record->name}}</option>
            @endforeach
        </select>

        @error($name)
        <span class="text-danger">{{ $errors->first($name) }}</span>
        @enderror

</div>
