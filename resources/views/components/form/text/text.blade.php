<div>
        <label class="label-font">{{$label}}</label>
        <input type="{{$type}}" id="{{$id}}" name="{{$name}}" class="form-control mb-3 {{$class}}"
               placeholder="{{$placeHolder}}" value="{{$value}}" wire:model="{{$name}}" >

        @error($name)
        <span class="text-danger">{{ $errors->first($name) }}</span>
        @enderror

</div>
