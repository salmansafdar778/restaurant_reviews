<!-- start sidebar menu -->
<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu">
        <div id="remove-scroll">
            <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true"
            data-slide-speed="200">

                <li class="nav-item">
                    <a class="nav-link nav-toggle">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                        <span class="title">Restaurants</span>
                        <span class="arrow"></span>

                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item">
                            <a href="{{route('restaurant.create')}}" class="nav-link">
                                <span class="title">Add Restaurant</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('restaurants')}}" class="nav-link">
                                <span class="title">View Restaurant</span>
                            </a>
                        </li>


                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{ route('comments') }}" class="nav-link">
                        <i class="fa fa-tasks" aria-hidden="true"></i>
                        <span class="title">Comments</span>
                        <span class="selected"></span>
                    </a>
                </li>

                @role('SuperAdmin')

            <li class="nav-item">
                <a class="nav-link nav-toggle">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span class="title">Users</span>
                    <span class="arrow"></span>

                </a>
                <ul class="sub-menu">
                    <li class="nav-item">
                        <a href="{{route('user.create')}}" class="nav-link">
                            <span class="title">Add User</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('user.all')}}" class="nav-link">
                            <span class="title">View Users</span>
                        </a>
                    </li>

                </ul>
            </li>
            @endrole

            <li>
                <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                <i class="fa fa-power-off" aria-hidden="true"></i> Log Out </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
            </li>

            </ul>
        </div>
    </div>
</div>
<!-- end sidebar menu -->
