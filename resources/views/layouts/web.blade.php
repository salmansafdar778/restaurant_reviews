<!DOCTYPE html>
<html lang="en">
<!--begin::Head-->

<head>
	<base href="">
	<meta charset="utf-8" />
	<title>Review Restaurants | @yield('title')</title>
	<meta name="description" content="Standard HTML" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<!--end::Layout Themes-->
	<link rel="shortcut icon" href="{{asset('assets/media/logos/fav.png')}}" />
	<link href="{{asset('assets/css/custom-style.css')}}" rel="stylesheet" type="text/css" />
</head>
<!--end::Head-->
<!--begin::Body-->
<style>
	body,html,.row,.container-fluid {
		height: 100%
	}

</style>
<body id="kt_body">

<div class="container-fluid">
<div class="row">

	<div class="col-md-12 bg-img">
		 <div class="container flex">
		 	<div class="footer_logo my-1 mb-4">
                <img src="{{asset('assets/img/ic_nitblogo.png')}}">
            </div>
            @yield('content')
        <!-- /card-container -->
			 <div class="bottom-text">
			 	<span class="border-footer"></span>
               <p>Powered by <?php echo date("Y"); ?> &copy;</p>
               	<span class="border-footer"></span>
            </div>


    </div><!-- /container -->
	</div>
</div>

</div>

</body>
<!--end::Body-->

</html>
