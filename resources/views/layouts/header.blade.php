<!-- start header -->
<div class="page-header navbar navbar-fixed-top">
<div class="page-header-inner">
    <!-- logo start -->
    <div class="page-logo">
        <img id="logo" src="{{asset('assets/img/headerlogo.png')}}" alt="" class="mt-0">
    </div>
    <ul class="nav navbar-nav navbar-left in">
        <li><a href="#" class="menu-toggler sidebar-toggler"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
    </ul>
    <h4 class="nitb-tittle">Review Restaurants</h4>
    <!-- start mobile menu -->
    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
        data-target=".navbar-collapse">
    <span></span>
    </a>
    <!-- end mobile menu -->
    <!-- start header menu -->
    <div class="top-menu">
        <ul class="nav navbar-nav pull-right">
            <!-- start manage user dropdown -->
            <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                data-close-others="true">
            <span class="username username-hide-on-mobile"> {{Auth::user()->name}} </span>
            <img alt="" class="img-circle " src="{{Auth::user()->profile ? Auth::user()->profile->attachment_id : '/images/profile.jpg'}}" />
            </a>
            <ul class="dropdown-menu dropdown-menu-default animated jello">
                <li>
                    <a href="#">
                    <i class="fa fa-cog" aria-hidden="true"></i> Settings </a>
                </li>
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off" aria-hidden="true"></i> Log Out
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>

                </li>
            </ul>
            </li>

        </ul>
    </div>
</div>
</div>
<!-- end header -->
