<!-- start js include path -->
<script src="{{asset('assets/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/app.js')}}"></script>
<script src="{{asset('assets/js/layout.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@livewireScripts


<script type="text/javascript">
    $(document).ready( function () {
        // $('#table_id').DataTable();
    } );

    var table = $('#table_id').DataTable( {
        ordering:  false
    });

</script>

<script type="text/javascript">
      	$(document).ready(function(){

            $('.nav-list li a').click(function(){
                $('li a').removeClass("active");
                $(this).addClass("active");
            });
        });

        function showAlertMasg(icon,title,text,footer=false){

            Swal.fire({
                icon: icon,
                title: title,
                text: text,
                footer: footer
            });

        }//end of function
</script>

@if (session()->has('success'))
    <script type="text/javascript">
    var masg = '<?php echo session('message') ;?>';
    showAlertMasg('success','Success',masg);
    </script>
@endif

@if(session()->has('error'))
    <script type="text/javascript">
        var masg = '<?php echo session('message'); ?>';
        showAlertMasg('error','Error',masg);
    </script>
@endif

@if(Session::has('success'))
<script type="text/javascript">
    var masg = '<?php echo Session::get('success') ;?>';
    showAlertMasg('success','Success',masg);
</script>
@endif

@if(Session::has('error'))
<script type="text/javascript">
    var masg = '<?php echo Session::get('error'); ?>';
    showAlertMasg('error','Error',masg);
</script>
@endif
