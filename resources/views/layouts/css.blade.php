<link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
      <!--bootstrap -->
      <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{asset('assets/css/pages/animate_page.css')}}" rel="stylesheet">
      <!-- Template Styles -->
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
      <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{asset('assets/css/theme-color.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{asset('assets/css/custom-style.css')}}" rel="stylesheet" type="text/css" />

@livewireStyles
