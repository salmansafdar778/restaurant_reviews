@extends('layouts.main')
@section('title','Edit Restaurant')
@section('breadcrumb','Edit Restaurant')

@section('content')

<div class="row affix-row">

    <div class="col-sm-12 col-md-12">
    <div class="affix-content">
        @livewire('restaurants',['restaurant'=>$restaurant, 'isEdit'=>true])
    </div>
    </div>
</div>


@endsection

@section('jsfile')


@endsection
