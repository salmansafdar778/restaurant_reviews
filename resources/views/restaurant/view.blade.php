@extends('layouts.main')
@section('title','View Restaurant')
@section('breadcrumb','View Restaurant')

@section('content')

<div class="row affix-row">

    <div class="col-sm-12 col-md-12">
    <div class="affix-content">

        <p>Name : {{$restaurant->name}}</p>
        <p>Avg Rating : {{$restaurant->avg_rating}}</p>
        <p>Highest Rating : {{$restaurant->hig_rating}}</p>
        <p>Lowest Rating : {{$restaurant->low_rating}}</p>
        <p>Last Review :</p>

        @if($restaurant->latestComment)
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">User Name</th>
                <th scope="col">Rating</th>
                <th scope="col">Comment</th>
                <th scope="col">Reply</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>{{$restaurant->latestComment->comment_user->name ?? ''}}</td>
                <td>{{$restaurant->latestComment->rating ?? ''}}</td>
                <td>{{$restaurant->latestComment->comment ?? ''}}</td>
                <td>{{$restaurant->latestComment->reply ?? ''}}</td>
            </tr>

            </tbody>
        </table>
        @endif

    </div>
    </div>
</div>


@endsection

@section('jsfile')


@endsection
