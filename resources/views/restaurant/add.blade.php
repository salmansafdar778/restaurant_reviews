@extends('layouts.main')
@section('title','Add Restaurant')
@section('breadcrumb','Add Restaurant')

@section('content')

    @if (session()->has('message'))
        <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3" role="alert">
            <div class="flex">
                <div>
                    <p class="text-sm">{{ session('message') }}</p>
                </div>
            </div>
        </div>
    @endif

    <div class="row affix-row">

        <div class="col-sm-12 col-md-12">
            <div class="affix-content">
                @livewire('restaurants')
            </div>
        </div>
    </div>

@endsection

@section('jsfile')

@endsection
