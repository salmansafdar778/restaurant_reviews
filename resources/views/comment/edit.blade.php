@extends('layouts.main')
@section('title','Add Reply')
@section('breadcrumb','Add Reply')

@section('content')

<div class="row affix-row">

    <div class="col-sm-12 col-md-12">
    <div class="affix-content">

        <form class="pt-4" method="POST" action="{{ route('comment.save_reply',$restaurant->id) }}" enctype='multipart/form-data'>
            @csrf
            @method('PUT')
            <div class="row">

                <div class="col-lg-6">
                <label class="label-font">Reply</label>
                    <textarea class="form-control" name="reply" id="reply" rows="3">{{old('reply')}}</textarea>

                @if ($errors->has('reply'))
                        <span class="text-danger">{{ $errors->first('reply') }}</span>
                @endif
                </div>

                <div class="col-lg-12 btn-submit mt-2 mb-2 ">
                <a type="button" class="btn btn-secondary" href="{{route('restaurants')}}">Cancel</a>
                <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        </form>
    </div>
    </div>
</div>


@endsection

@section('jsfile')


@endsection
