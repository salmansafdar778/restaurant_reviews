@extends('layouts.main')
@section('title','Add Restaurant')
@section('breadcrumb','Add Restaurant')

@section('content')

<div class="row affix-row">

    <div class="col-sm-12 col-md-12">
    <div class="affix-content">

        <form class="pt-4" method="POST" action="{{ route('restaurant.store') }}" enctype='multipart/form-data'>
            @csrf
            <div class="row">

                <div class="col-lg-6">
                <label class="label-font">Name</label>
                <input type="text" id="name" name="name" class="form-control mb-3"
                placeholder="Name" required="" value="{{old('name')}}">
                @if ($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                </div>

                <div class="col-lg-12 btn-submit mt-2 mb-2">
                <button type="button" class="btn btn-secondary">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </form>
    </div>
    </div>
</div>


@endsection

@section('jsfile')

@endsection
