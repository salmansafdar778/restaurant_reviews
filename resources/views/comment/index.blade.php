@extends('layouts.main')
@section('title','All Restaurants')
@section('breadcrumb','All Restaurants')

@section('content')

<table id="table_id" class="display all-user dataTable no-footer">
<thead>
<tr>
    <th>Sr.</th>
    <th>Restaurant</th>
    <th>Comment</th>
    <th>Reply</th>
    <th>Rating</th>
    <th>User Name</th>
    <th>Actions</th>
</tr>
</thead>
<tbody>


</tbody>
</table>

@endsection

@section('jsfile')

<script>
    $(function () {

    table.destroy();
    table = $('#table_id').DataTable({
        stateSave: true,
        processing: true,
        serverSide: true,
        ordering:  false,
        ajax: "{{ route('comment.getComments') }}",
        "order": [],
        'columnDefs': [{ 'orderable': false, 'targets': 0 }],
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex',orderable: false, searchable: false},
            {data: 'restaurant', name: 'restaurant'},
            {data: 'comment', name: 'comment'},
            {data: 'reply', name: 'reply'},
            {data: 'rating', name: 'rating'},
            {data: 'user_name', name: 'user_name'},
            {data: 'action', name: 'action'},
        ]
    });

  });

    function deleteR(id) {
    event.preventDefault();

console.log(id);
    var url = $("#delete_"+id).attr('href');
   console.log(url);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn btn-danger ml-1',
      buttonsStyling: false,
    }).then(function (result) {
      console.log(result);
      if (result.value) {
        console.log("yes delete it.");
        window.location.href = url;
      }
      else if (result.dismiss === Swal.DismissReason.cancel) {
        console.log("cancel.");

      }
    });
      // if(!confirm("Are You Sure to delete this"))
      //     event.preventDefault();
  }
</script>

@endsection
