<?php

namespace App\Http\Controllers;

use App\Http\Requests\RestaurantRequest;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Models\User;
use App\Models\Restaurant;

use Auth;
use DB;

class RestaurantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }//end of function

    public function index(Request $request){

        $data = $request->all();
        return view('restaurant.index');
    }

    public function getRestaurant(Request $request){

        $data = $request->all();
        if (Auth::User()->hasRole('SuperAdmin')) {
            $restaurants = Restaurant::withCount(['comments as average_rating' => function($query) {
                $query->select(DB::raw('coalesce(avg(rating),0)'));
            }])->orderBy('average_rating','DESC')->get();
        }else{
            $restaurants = Restaurant::where('user_id',Auth::user()->id)
            ->withCount(['comments as average_rating' => function($query) {
                $query->select(DB::raw('coalesce(avg(rating),0)'));
            }])->orderBy('average_rating','DESC')->get();
        }

    	return Datatables::of($restaurants)
                ->addIndexColumn()
                ->addColumn('name', function($row){
                    return $row->name;
                })->addColumn('avg_rating', function($row){
                    return $row->avg_rating;
                })->addColumn('action', function($row){

                    $actionBtn = '<a href="'.route('restaurant.view',$row->id).
                        '" class="btn btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></a> <a href="'.route('restaurant.edit',$row->id).
                    '" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i></a> <a href="'.route('restaurant.delete',$row->id).
                    '" onClick="return deleteR('.$row->id.');" id="delete_'.$row->id.
                    '" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);

    }//end of function

    public function create(Request $request){

        $data = $request->all();
        return view('restaurant.add');

    }//end of function

    public function store(RestaurantRequest $request){

        $restaurantData = $request->validated();
        DB::beginTransaction();

        try {

            $record = new Restaurant();
            $record->name = $request->name;
            $record->user_id = Auth::user()->id;
            $record->save();

            DB::commit();
            return redirect()->route('restaurants')->with('success', 'Restaurant created successfully.');

        }catch (\Exception $e){
            DB::rollback();
            return $e->getMessage();
            return  redirect()->back()->with('error', "Something went wrong please try agian.");

        }

    }//end of function

    public function view(Request $request,$id){
        $data = $request->all();
        $restaurant = Restaurant::find($id);
        return view('restaurant.view',compact(['restaurant']));
    }//end of function

    public function edit(Request $request,$id){
        $data = $request->all();
        $restaurant = Restaurant::find($id);

        return view('restaurant.edit',compact(['restaurant']));
    }//end of function

    public function update(Request $request ,RestaurantRequest $restaurantRequest,$id){

        $restaurantData = $restaurantRequest->validated();
        DB::beginTransaction();

        try {

            $restaurant = Restaurant::find($id);
            $restaurant->fill($restaurantData)->save();

            DB::commit();
            return redirect()->route('restaurants')->with(['success'=>'Restaurant updated successfully.']);

        }catch (\Exception $e){
            DB::rollback();
            return $e->getMessage();

            return redirect()->route('restaurants')->with(['error'=>'Something went wrong please try again.']);
        }
    }//end of function

    public function delete(Request $request,$id){
        $data = $request->all();

        $record = Restaurant::find($id);
        $record->delete();

        return redirect()->route('restaurants')->with('Restaurant deleted successfully.');
    }//end of function
}
