<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Mail;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Notifications\AppNotification;

use App\Models\Attachment;
use App\Models\Application;
use App\Models\Subcategory;
use App\Models\User_Application_Status;
use App\Models\User;

use Carbon\Carbon;
use Validator;
use Auth;
// use Mail;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function send_mail($data,$view){

         $data['from_email'] = env('MAIL_FROM_ADDRESS');

         try{

             Mail::send($view,array('data'=>$data), function($message) use ($data){
             $message->from($data['from_email']);
             $message->to($data['to_email']);
             $message->subject($data['subject']);

             });

             if (Mail::failures()) {
                 return 0;
             }else{
                 return 1;
             }
         } catch (\Exception $e) {

            return $e->getMessage();
             return 0;
         }

    }//end of send_mail

    public function unique_code($limit)
    {
      return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }//end of function

    public function save_notification($sender_id,$title,$description,$action,$action_id){

        $admins = User::whereHas(
            'roles', function($q){
                $q->where('name', 'Administrator');
            }
        )->get();

        foreach ($admins as $key => $u) {

            $user = User::find($u->id);
            $user->notify(new AppNotification($sender_id,$user->id,$title,$description,$action,
            $action_id));

        }//end of foreach

    }//end of function

    public function save_application($category_id,$subcategory_id,$entity_id,$entity_name,
    $application_status_id,$submission_date,$from_user_id,$entity=null){

        $application = new Application();

        $application->category_id=$category_id;
        $application->subcategory_id=$subcategory_id;
        // $application->entity_id=$entity_id;
        $application->entity_name=$entity_name;
        $application->application_status_id=$application_status_id;
        $application->submission_date=Carbon::now()->format('Y-m-d');
        $application->from_user_id=$from_user_id;
        // $application->to_user_id=$from_user_id;

        // $application->save();
        $entity->application()->save($application);

        $application_status = new User_Application_Status();

        $application_status->remarks=null;
        $application_status->date=Carbon::now()->format('Y-m-d');
        $application_status->is_active=1;
        $application_status->application_id=$application->id;
        $application_status->from_user_id=$from_user_id;
        // $application_status->to_user_id=$to_user_id;
        $application_status->status_id=$application_status_id;
        $application_status->save();

        return $application;

    }//end of function

    public function save_noc_status($data,$application){

        $application_status = new User_Application_Status();

        $application_status->remarks=$data['remarks'];
        $application_status->date=Carbon::now()->format('Y-m-d');
        $application_status->is_active=$data['is_active'];
        $application_status->application_id=$data['application_id'];
        $application_status->from_user_id=$data['from_user_id'];
        $application_status->to_user_id=$data['to_user_id'];
        $application_status->status_id=$data['status_id'];
        $application_status->save();

        $user = User::find($data['to_user_id']);
        $from_user = User::find($data['from_user_id']);

        $title = 'Application Status Changed';
        $description = $from_user->name.' change this application '.
        $application->category->name.' status to '.$application->status_name;
        $action = 'application_status_changed';
        $action_id = $application->id;

        $user->notify(new AppNotification($data['from_user_id'],$user->id,$title,$description,$action,
        $action_id));

        return $application_status;

    }//end of function

    public function save_attachment($request,$field,$type,$folder){

        $attachment_id =0;
        if ($request->hasFile($field)) {

            $cover = $request->$field;
            $extension = uniqid() . "." . $cover->getClientOriginalName();
            $path="/".$folder."/" . $extension;
            Storage::disk('public')->put($path, File::get($cover));

            $attachment = new Attachment();
            $attachment->path = $path;
            $attachment->save();

            $attachment_id = $attachment->id;
        }

        return $attachment_id;

    }//end of function

    public function save_multiple_attachment($file,$type,$folder){

        $attachment_id =0;
        $cover = $file;
        $extension = uniqid() . "." . $cover->getClientOriginalName();
        $path="/".$folder."/" . $extension;
        Storage::disk('public')->put($path, File::get($cover));

        $attachment = new Attachment();
        $attachment->path = $path;
        $attachment->save();

        $attachment_id = $attachment->id;

        return $attachment_id;

    }//end of function

    public function update_attachment($request,$attachment_id,$field,$type,$folder){

        if ($request->hasFile($field)) {

            $cover = $request->$field;
            $extension = time() . "." . $cover->getClientOriginalName();
            $path="/".$folder."/" . $extension;
            Storage::disk('public')->put($path, File::get($cover));

            $attachment = Attachment::where('id',$attachment_id)->first();

            if(empty($attachment)){
                $attachment->path = $path;
                $attachment->save();
            }
        }

        return $attachment_id;

    }//end of function

    public function getSubCategory($sub_id){

        $subcategory = Subcategory::find($sub_id);
        return $subcategory;

    }//end of function

    public function getUserCanWrite($application){

        if (Auth::User()->hasRole('Moderator')) {

            $subcategory = Subcategory::find($application->subcategory_id);
            $write_id = $subcategory->sub_permission('write')->first()->id ?? '';

            if(Auth::user()->hasPermissionTo($write_id) == 1){
                return true;
            }

            return false;
        }else{
            return true;
        }

    }//end of function

    public function getModeratorSubcate(){

        $subcategories = array();
        if (Auth::User()->hasRole('Moderator')) {

            $permissions = Auth::user()->getAllPermissions('subcategory_id')
            ->unique('subcategory_id');

            foreach ($permissions as $key => $permission) {
                if($permission->subcategory_id){
                    $subcategories[] = $permission->subcategory_id;
                }

            }

        }

        return $subcategories;

    }//end of function
}
