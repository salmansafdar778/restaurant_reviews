<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Yajra\DataTables\Facades\DataTables;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UpdateUserRequest;

use App\Models\User;
use App\Models\Forget_Password;

use Auth;
use DB;

use Illuminate\Support\Str;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }//end of function

    public function index(Request $request){
        $data = $request->all();

    }//end of function

    public function all_users(Request $request){

        $data = $request->all();
        return view('users.view_users');

    }//end of function

    public function getUsers(Request $request){

        $data = $request->all();
        $users = User::orderBy('id','DESC')->whereHas(
            'roles', function($q){
                $q->where('name','User');
            }
        )->get();

    	return Datatables::of($users)
                ->addIndexColumn()
                ->addColumn('name', function($row){
                    return $row->name ?? '';
                })->addColumn('email', function($row){
                    return $row->email;
                })->addColumn('action', function($row){

                    $actionBtn = '<a href="'.route('user.edit',$row->id).
                    '" class="btn btn-warning">
                    <i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <a href="'.route('user.delete',$row->id).
                    '" onClick="return deleteR('.$row->id.');" id="delete_'.$row->id.
                    '" class="btn btn-danger">
                    <i class="fa fa-trash-o" aria-hidden="true"></i></a>';

                    return $actionBtn;
                })->filter(function ($instance) use ($request) {

                }, true)
                ->rawColumns(['action'])
                ->make(true);

    }//end of function

    public function create(Request $request){
        $data = $request->all();
        $roles = Role::where('name','=','User')->get();

        return view('users.add_user',compact(['roles']));
    }//end of function

    public function store(Request $request,UserRegisterRequest $userRequest){

        $userData = $userRequest->validated();

        DB::beginTransaction();

        try {

        	$user = User::create([
                'name' => $userRequest->name,
                'email' => $userRequest->email,
                'password' => $userRequest->password,
            ]);

            $user->assignRole($request->role);
            DB::commit();
            return redirect()->route('user.all')->with('success', 'User created successfully.');

        }catch (\Exception $e){
            DB::rollback();
            return $e->getMessage();
            return  redirect()->back()->with('error', "Something went wrong please try agian.");

        }

    }//end of function

    public function edit(Request $request,$id){
        $data = $request->all();
        $roles = Role::where('name','=','User')->get();
        $user = User::find($id);

        return view('users.edit_user',compact(['user','roles']));
    }//end of function

    public function update(Request $request ,UpdateUserRequest $userRequest,$id){
        $userData = $userRequest->validated();

        DB::beginTransaction();
        try {

            $user = User::find($id);

            if($userRequest->name){
                $user->name = $userRequest->name;
            }

            if($userRequest->password){
                $user->password = $userRequest->password;
            }

            if($userRequest->email){
                $user->email = $userRequest->email;
            }

            $user->save();
            DB::commit();
            return redirect()->route('user.all')->with(['success'=>'User updated successfully.']);

        }catch (\Exception $e){
            DB::rollback();
            // return $e->getMessage();

            return redirect()->route('user.all')->with(['error'=>'Something went wrong please try again.']);
        }
    }//end of function

    public function delete(Request $request,$id){
        $data = $request->all();

        $user = User::find($id);
        $user->delete();

        return redirect()->route('user.all')->with('User deleted successfully.');
    }//end of function

}
