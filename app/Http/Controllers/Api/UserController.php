<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Models\User;
use App\Models\User_Profile;
use App\Models\Attachment;
use App\Models\Forget_Password;
use App\Models\Address;
use App\Models\Event;

use Validator;
use Auth;
use Mail;
use DB;

use App\Http\Resources\User as UserResource;
use App\Http\Resources\Dashboard as DashboardResource;
use App\Http\Resources\Notification;
use App\Http\Resources\NotificationCollection;
use App\Http\Resources\EventCollection;

class UserController extends ApiController
{
    public function userProfile(Request $request) {

        $data = $request->all();
        try{

            $user_id = isset($data['user_id']) ? $data['user_id'] : 0;

            if(Auth::check()){
                $user_id = Auth::user()->id;
            }

            $user = User::find($user_id);
    		return response()->json(['data' => ['user'=>new UserResource($user)],'meta'=> ['message' => 'Successfully get the user profile','status'=> 200,'errors'=>null]]);

        }catch (\Exception $e){
            return response()->json(['data' => null,'meta'=> ['message' => $e->getMessage(),'status'=> 402,'errors'=>null]]);

        }//end of try

    }//end of function

    public function updateProfile(Request $request) {

        $data = $request->all();

        $user_id = 0;
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }

        $user = User::find($user_id);
        $validator = Validator::make($request->all(), [
            'name' => 'nullable|string|max:255',
            'email' => 'nullable|string|email|max:255|unique:users,id,'. $user_id,
        ]);

        if ($validator->fails()) {
            return response()->json(['data' => null,'meta'=> ['message' => 'Validation Error','status'=> 402,'errors'=>$validator->errors()]]);
        }

        DB::beginTransaction();

        try{

            if ($request->has('name')) {
			    $user->name = $request->name;
			}

			if ($request->has('email')) {
			    $user->email = $request->email;
			}

            $user->save();
            DB::commit();
            return response()->json(['data' => ['user'=>new UserResource($user)],'meta'=> ['message' => 'Successfully updated the user profile','status'=> 200,'errors'=>null]]);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['data' => null,'meta'=> ['message' => $e->getMessage(),'status'=> 402,'errors'=>null]]);

        }//end of try

    }//end of function

    public function updatePassword(Request $request) {

        $data = $request->all();

        $user_id = 0;
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }

        $user = User::find($user_id);

        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed',
            'current_password' => 'required|password',

        ]);

        if ($validator->fails()) {
            return response()->json(['data' => null,'meta'=> ['message' => 'Validation Error','status'=> 402,'errors'=>$validator->errors()]]);
        }

        DB::beginTransaction();

        try{

            if ($request->has('password')) {
			    $user->password = $request->password;
			}
			$user->save();

            DB::commit();
            return response()->json(['data' => ['user'=>new UserResource($user)],'meta'=> ['message' => 'Successfully updated the password.','status'=> 200,'errors'=>null]]);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['data' => null,'meta'=> ['message' => $e->getMessage(),'status'=> 402,'errors'=>null]]);

        }//end of try

    }//end of function

}
