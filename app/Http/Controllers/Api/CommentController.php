<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use App\Models\Restaurant_Review;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Models\User;

use Validator;
use Auth;
use Mail;
use DB;

use App\Http\Resources\Comment as CommentResouce;
use App\Http\Resources\CommentCollection;


class CommentController extends ApiController
{
    public function add(Request $request) {

        $data = $request->all();
        $user_id = 0;

        $validator = Validator::make($request->all(), [
            'restaurant_id' => 'required|exists:restaurants,id',
            'comment' => 'required|string|max:255',
            'rating' => 'required|numeric|max:5',
        ]);
        if ($validator->fails()) {
            return response()->json(['data' => null,'meta'=> ['message' => 'Validation Error','status'=> 402,'errors'=>$validator->errors()]]);
        }

        DB::beginTransaction();

        try{

            if(Auth::check()){
                $user_id = Auth::user()->id;
            }

            $comment = Restaurant_Review::create([
                'comment_user_id' => $user_id,
                'restaurant_id' => $request->restaurant_id,
                'comment' => $request->comment,
                'rating' => $request->rating,
            ]);

            DB::commit();
            return new CommentResouce($comment);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['data' => null,'meta'=> ['message' => $e->getMessage(),'status'=> 402,'errors'=>null]]);

        }//end of try

    }//end of addComment
}
