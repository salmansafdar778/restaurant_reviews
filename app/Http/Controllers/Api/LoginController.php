<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Forget_Password;
use Validator;
use Auth;
use Mail;
use DB;

use App\Http\Resources\User as UserResource;

class LoginController extends ApiController
{
    public function index(Request $request){

        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['data' => null,'meta'=> ['message' => 'Validation Error','status'=> 402,'errors'=>$validator->errors()]]);
        }

        $user=User::where('email',request('email'))->orWhere('name',request('email'))->count();
        if($user!=0)
        {
            $identity = request('email');

            //'active'=>1
            if(Auth::attempt([filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'name' => request('email'), 'password' => request('password')])){
                $user = Auth::user();

                if(!Auth::User()->hasRole('User')){

                    Auth::logout();
                    return response()->json(['data' => null,'meta'=> ['message' => 'Validation Error','status'=> 402,'errors'=>['password' => 'User not found.']]]);
                }

                $tokenResult = $user->createToken('Personal Access Token');
                $token = $tokenResult->token;

                if ($request->remember_me)
                    $token->expires_at = Carbon::now()->addWeeks(1);
                	$token->save();

                $user->save();

                if (!Auth::User()->hasRole('User')) {
                    $user->assignRole('User');
                }

                $success=[
                    'access_token' => $tokenResult->accessToken,
                    'user'         =>new UserResource($user),

                ];

                return response()->json(['data' => $success,'meta'=> ['message' => 'Successfully logged in','status'=> 200,'errors'=>null]]);

            }
            else{
                return response()->json(['data' => null,'meta'=> ['message' => 'Validation Error','status'=> 402,'errors'=>['password' => 'Password Not Matched']]]);

            }
        }
        else
        {
            return response()->json(['data' => null,'meta'=> ['message' => 'Validation Error','status'=> 402,'errors'=>['email' => 'Email Not Found']]]);

        }

    }//end of function

    public function logout(Request $request){

        $data= $request->all();
        $user_id = 0;
        DB::beginTransaction();

        try{

            if(Auth::check()){

                $token = Auth::user()->token();
                $token->revoke();
            }

            DB::commit();
            return response()->json(['data' => null,'meta'=> ['message' => 'Logged out.','status'=> 200,'errors'=>null]]);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['data' => null,'meta'=> ['message' => $e->getMessage(),'status'=> 402,'errors'=>null]]);

        }//end of try

    }//end of function

    public function forgetPassword(Request $request) {

    	$data = $request->all();

        $validator = Validator::make($data, [
            'email' => 'required|email|exists:users',
        ]);

        if ($validator->fails()) {
            return response()->json(['data' => null,'meta'=> ['message' => 'User Not Found',
            'status'=> 402,'errors'=>$validator->errors()]]);
        }

        try {

            $passwordEmail = $request->email;

            $user=User::where('email',$request->email)->firstOrFail();
            $code=rand(1000,9999);

            session()->forget('passwordCode');
            session()->forget('passwordEmail');

            session(['passwordEmail' => $passwordEmail, 'passwordCode' => $code]);

            $forgetPassword = Forget_Password::where('user_id',$user->id)->first();

            if(empty($forgetPassword)){

                $forgetPassword = Forget_Password::create([
                    'verification_code'=>$code,
                    'user_id'=>$user->id,
                ]);

            }else{
                $forgetPassword->verification_code  = $code;
                $forgetPassword->save();
            }

            $mail_data['subject'] = "Reset Password!";
            $mail_data['to_email'] = $user->email;
            $mail_data['code'] = $code;

            $res = $this->send_mail($mail_data,'emails.password_reset');
            return response()->json(['data' => ['user_id'=>$user->id],'meta'=> ['message' => 'Please check your email.','status'=> 200,'errors'=>null]]);

        }catch (\Exception $e){

            return response()->json(['data' => null,'meta'=> ['message' => $e->getMessage(),'status'=> 402,'errors'=>null]]);
        }

    }//end of function

    public function verifyCode(Request $request){

        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'code' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['data' => null,'meta'=> ['message' => 'Validation Error','status'=> 402,'errors'=>$validator->errors()]]);

        }

        $code = $request->code;
        $forgetPassword = Forget_Password::where('verification_code',$request->code)->first();

        if(!empty($forgetPassword)){
            return response()->json(['data' => ['user_id'=>$forgetPassword->user_id],'meta'=> ['message' => 'User successfully verify.','status'=> 200,'errors'=>null]]);
        }else{
            return response()->json(['data' => null,'meta'=> ['message' => "User not found.",'status'=> 402,'errors'=>null]]);
        }

    }//end of function

    public function resetPassword(Request $request){

        $data = $request->all();

        $validated = $request->validate([
            'password' => 'required|confirmed',
            'user_id' => 'required'
        ]);

        $password = $request->password;
        // $user = User::where('email', $request->email)->first();
        $user = User::where('id', $request->user_id)->first();

        if(!empty($user)){

            // $user->password = \Hash::make($password);
            $user->password = $password;


            $forgetPassword = Forget_Password::where('user_id',$user->id)->first();
            if(!empty($forgetPassword)){
                $user->save();
                $forgetPassword->delete();
            }else{
                return response()->json(['data' => null,'meta'=> ['message' => "User not found.",'status'=> 402,'errors'=>null]]);
            }

            return response()->json(['data' => ['user_id'=>$user->id],'meta'=> ['message' => 'Password changed successfully.','status'=> 200,'errors'=>null]]);
        }else{
            return response()->json(['data' => null,'meta'=> ['message' => "User not found.",'status'=> 402,'errors'=>null]]);
        }

    }//end of function

    public function resendCode(Request $request){

        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['data' => null,'meta'=> ['message' => 'Validation Error','status'=> 402,'errors'=>$validator->errors()]]);

        }

        $user = User::where('id',$request->user_id)->first();

        if(!empty($user)){

            $mail_data['subject'] = "Resend Password Code!";
            $mail_data['to_email'] = $user->email;
            $code=rand(1000,9999);
            $mail_data['code'] = $code;

            $forgetPassword = Forget_Password::where('user_id',$user->id)->first();
            if(empty($forgetPassword)){

                $forgetPassword = Forget_Password::create([
                    'verification_code'=>$code,
                    'user_id'=>$user->id,
                ]);

            }else{
                $forgetPassword->verification_code  = $code;
                $forgetPassword->save();
            }

            $res = $this->send_mail($mail_data,'emails.password_reset');
            return response()->json(['data' => ['user_id'=>$user->id],'meta'=> ['message' => 'Please check your email.','status'=> 200,'errors'=>null]]);


        }else{
            return response()->json(['data' => null,'meta'=> ['message' => "User not found.",'status'=> 402,'errors'=>null]]);
        }

    }//end of function

}
