<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Models\User;

use Validator;
use Auth;
use Mail;
use DB;

use App\Http\Resources\Restaurant as RestaurantResource;
use App\Http\Resources\Comment as CommentResource;
use App\Http\Resources\RestaurantCollection;
use App\Http\Resources\CommentCollection;


class RestaurantController extends ApiController
{
    public function show(Request $request,$id) {

        $data = $request->all();
        $restaurant = Restaurant::find($id);

        if(!empty($restaurant)){
            return response()->json(['data' => ['restaurant'=>new RestaurantResource($restaurant)],'meta'=> ['message' => 'Successfully show the restaurant.','status'=> 200,'errors'=>null]]);
        }else{
            return response()->json(['data' => null,'meta'=> ['message' => 'Restaurant not found','status'=> 402,'errors'=>null]]);
        }


    }//end of function

    public function getComment(Request $request,$id) {

        $data = $request->all();
        $restaurant = Restaurant::find($id);

        if(!empty($restaurant)){
            return response()->json(['data' => ['restaurant'=>CommentResource::collection($restaurant->comments)],
                'meta'=> ['message' => 'Successfully show the restaurant comments.','status'=> 200,'errors'=>null]]);
        }else{
            return response()->json(['data' => null,'meta'=> ['message' => 'Restaurant not found','status'=> 402,'errors'=>null]]);
        }


    }//end of function

    public function all(Request $request) {

        $data = $request->all();
        $perpage = isset($data['per_page']) ? $data['per_page'] : 10;

        $restaurants = Restaurant::withCount(['comments as average_rating' => function($query) {
            $query->select(DB::raw('coalesce(avg(rating),0)'));
        }])->orderBy('average_rating','DESC')->paginate($perpage);
        return new RestaurantCollection($restaurants);


    }//end of function
}
