<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Resources\User as UserResource;

use App\Models\User;
use Validator;
use Auth;
use Config;
use DB;

class SignupController extends ApiController
{
    public function index(Request $request) {
        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(['data' => null,'meta'=> ['message' => 'Validation Error','status'=> 402,'errors'=>$validator->errors()]]);
        }

        DB::beginTransaction();

        try {

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
            ]);

            $user->assignRole('User');

             $tokenResult = $user->createToken('Personal Access Token');
             $success['access_token'] =   $tokenResult->accessToken;

            DB::commit();

             $user=User::find($user->id);
             $success['user'] =  new UserResource($user);

            return response()->json(['data' => $success,'meta'=> ['message' => 'Account created in successfully.','status'=> 200,'errors'=>null]]);

        }catch (\Exception $e){
            DB::rollback();
            // return $e->getMessage();
            return response()->json(['data' => null,'meta'=> ['message' => 'Something went wrong please try again.','status'=> 402,'errors'=>null]]);

        }

    }//end of function
}
