<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Restaurant_Review;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Models\User;
use App\Models\Restaurant;

use Auth;
use DB;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }//end of function

    public function index(Request $request){

        $data = $request->all();
        return view('comment.index');
    }

    public function getComments(Request $request){

        $data = $request->all();
        if (Auth::User()->hasRole('SuperAdmin')) {
            $restaurants = Restaurant_Review::orderBy('id','DESC')->get();
        }else{
            $restaurants = Restaurant_Review::whereHas('restaurant', function($q){
                $q->where('user_id', Auth::user()->id);
            })->orderBy('id','DESC')->get();
        }

    	return Datatables::of($restaurants)
                ->addIndexColumn()
                ->addColumn('restaurant', function($row) {
                    return $row->restaurant->name ?? '';
                })->addColumn('comment', function($row){
                    return $row->comment;
                })->addColumn('reply', function($row){
                return $row->reply;
            })->addColumn('rating', function($row){
                return $row->rating;
                })->addColumn('user_name', function($row){
                    return $row->comment_user->name ?? '';
                })->addColumn('action', function($row){

                    if($row->reply){

                        $actionBtn = '<a href="'.route('comment.delete',$row->id).
                            '" onClick="return deleteR('.$row->id.');" id="delete_'.$row->id.
                            '" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';

                    }else{
                        $actionBtn = '<a href="'.route('comment.add_reply',$row->id).
                            '" class="btn btn-warning"><i class="fa fa-commenting-o" aria-hidden="true"></i></a> <a href="'.route('comment.delete',$row->id).
                            '" onClick="return deleteR('.$row->id.');" id="delete_'.$row->id.
                            '" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';
                    }

                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);

    }//end of function

    public function add_reply(Request $request,$id){
        $data = $request->all();
        $restaurant = Restaurant_Review::find($id);

        return view('comment.edit',compact(['restaurant']));
    }//end of function

    public function save_reply(Request $request ,CommentRequest $commentRequest,$id){

        $commentData = $commentRequest->validated();
        DB::beginTransaction();

        try {

            $comment = Restaurant_Review::find($id);
            $comment->fill(['reply'=>$commentRequest->reply,'reply_user_id'=>Auth::user()->id])->save();

            DB::commit();
            return redirect()->route('comments')->with(['success'=>'Reply added successfully.']);

        }catch (\Exception $e){
            DB::rollback();
            return $e->getMessage();

            return redirect()->route('comments')->with(['error'=>'Something went wrong please try again.']);
        }
    }//end of function

    public function delete(Request $request,$id){
        $data = $request->all();

        $record = Restaurant_Review::find($id);
        $record->delete();

        return redirect()->route('restaurants')->with('Comment deleted successfully.');
    }//end of function
}
