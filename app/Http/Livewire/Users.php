<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use Spatie\Permission\Models\Role;
use Livewire\WithFileUploads;

use Auth;
use DB;

class Users extends Component
{

    use WithFileUploads;

    public $user,$name,$email,$password,$user_id,$password_confirmation,$role;
    public $isOpen = 0;
    public $isEdit = false;
    public $image;
    public $roles = [];

    public function mount(User $user,$isEdit=false,$role='')
    {
        $this->name = $user->name;
        $this->email = $user->email;
//        $this->password = $user->password;
        $this->user_id = $user->id;
        $this->role = $role;
        $this->roles = Role::where('name','=','User')->get();

    }

//    public function __construct(){
//
//    }

    protected function rules()
    {
        $pReq = 'required';
        if($this->isEdit == 1){
            $pReq = 'nullable';
        }

        return [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:100|unique:users,name,'.$this->user_id,
            'email' => 'required|string|email|max:200|unique:users,email,'.$this->user_id,
            'password' => $pReq.'|string|confirmed',
            'role' => 'required|exists:roles,name',
            'image' => 'nullable|image|max:1024',
        ];
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {

        $this->validate();

        DB::beginTransaction();

        try {

            if(empty($this->user)){
                $record = new User();
                $record->password = $this->password;
            }else{
                $record  = $this->user;
            }

            $record->name = $this->name;
            $record->email = $this->email;

            if($this->image){
                $imag = $this->image->storePublicly('profiles','public');
                $record->image = $imag ?? '';

            }

//            if($this->image){
//
//            }
            $record->save();

            if(empty($this->user)){
                $record->assignRole($this->role);
            }

            DB::commit();
            session()->flash('success',
                $this->user_id ? 'User Updated Successfully.' : 'User Created Successfully.');
            return redirect()->route('user.all');

        }catch (\Exception $e){
            DB::rollback();
            $isError = true;
            $masg = $e->getMessage();
//            $masg = 'Something went wrong please try again.';
            session()->flash('message',$masg);
        }
    }

    public function render()
    {
        return view('livewire.users');
    }
}
