<?php

namespace App\Http\Livewire;

use App\Http\Requests\RestaurantRequest;
use Livewire\Component;
use App\Models\Restaurant;

use Auth;
use DB;

class Restaurants extends Component
{

    public $restaurant,$name, $restaurant_id;
    public $isOpen = 0;
    public $isEdit = false;

    public function mount(Restaurant $restaurant,$isEdit=false)
    {
        $this->name = $restaurant->name;
        $this->restaurant_id = $restaurant->id;
    }

    public function render()
    {
        return view('livewire.restaurants');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function create()
    {
        $this->resetInputFields();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function openModal()
    {
        $this->isOpen = true;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function closeModal()
    {
        $this->isOpen = false;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields(){
        $this->name = '';
        $this->restaurant_id = '';
        $isError = false;
    }

//    protected $rules = [
//        'name' => 'required|unique:restaurants',
//    ];

    protected function rules()
    {
        return [
            'name' => 'required|unique:restaurants,name,'.$this->restaurant_id,
        ];
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {

        $this->validate();

        DB::beginTransaction();

        try {

            if(empty($this->restaurant)){
                $record = new Restaurant();
                $record->user_id = Auth::user()->id;
            }else{
                $record  = $this->restaurant;
            }

            $record->name = $this->name;
            $record->save();

            DB::commit();
            session()->flash('success',
                $this->restaurant_id ? 'Restaurant Updated Successfully.' : 'Restaurant Created Successfully.');
            return redirect()->route('restaurants');

        }catch (\Exception $e){
            DB::rollback();
//            return $e->getMessage();
            $isError = true;
            $masg = 'Something went wrong please try again.';
//            $masg = $e->getMessage();
            session()->flash('message',$masg);
        }
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit($id)
    {
        $restaurant = Restaurant::findOrFail($id);
        $this->restaurant_id = $id;
        $this->name = $restaurant->name;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete($id)
    {
        Restaurant::find($id)->delete();
        session()->flash('message', 'Restaurant Deleted Successfully.');
    }

    public function cancel()
    {
        return redirect()->route('restaurants');
    }

}
