<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Models\User_Profile;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required|regex:/^[\pL\s\-]+$/u|max:100',
            'password' => 'nullable|string|confirmed',
            'email' => 'required|string|email|max:200|unique:users,email,' . $this->id,
        ];
    }
}
