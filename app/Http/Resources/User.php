<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CurrentPosting as PostingResouce;
use App\Models\User_Profile;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'image'=>$this->image,
            'email_verified_at'=>$this->email_verified_at,
            'role' => $this->roles[0]->name,
        ];

    }

    public function with($r){
        return [
            'meta' => [
                'message' => 'Api Hit Success',
                'status'=>200,
                'errors'=>null,

            ]

        ];
    }
}
