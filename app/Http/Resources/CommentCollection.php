<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Storage;
use App\User;
use Auth;

use App\Http\Resources\User as UserResouce;

class CommentCollection extends ResourceCollection
{

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $pagination;

    public function __construct($resource)
    {
        $this->pagination = [
            'next' => $resource->nextPageUrl(),
            'previous' => $resource->previousPageUrl(),
            'more' => $resource->hasMorePages(),
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);

    }
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'data' => $this->collection->transform(function($comment){

                return [
                    'id'=>$comment->id,
                    'restaurant_id'=>$comment->restaurant_id,
                    'rating'=>$comment->rating,
                    'comment'=>$comment->comment,
                    'reply'=>$comment->reply,
                    'created_at'=>$comment->created_at,
                    'comment_user'=>$comment->comment_user->name ?? '',
                    'reply_user'=>$comment->reply_user->name ?? '',

                ];
            }),

            'pagination' => $this->pagination
        ];
    }

    public function with($r){
        return [
            'meta' => [
                'message' => 'Api Hit Success',
                'status'=>200,
                'errors'=>[],

            ]

        ];
    }

}
