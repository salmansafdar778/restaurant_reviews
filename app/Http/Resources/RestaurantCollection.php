<?php

namespace App\Http\Resources;

use App\Http\Resources\Comment as CommentResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Storage;
use App\User;
use Auth;

class RestaurantCollection extends ResourceCollection
{

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private $pagination;

    public function __construct($resource)
    {
        $this->pagination = [
            'next' => $resource->nextPageUrl(),
            'previous' => $resource->previousPageUrl(),
            'more' => $resource->hasMorePages(),
            'total' => $resource->total(),
            'count' => $resource->count(),
            'per_page' => $resource->perPage(),
            'current_page' => $resource->currentPage(),
            'total_pages' => $resource->lastPage()
        ];

        $resource = $resource->getCollection();

        parent::__construct($resource);

    }
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'data' => $this->collection->transform(function($restaurant){

                return [
                    'id'=>$restaurant->id,
                    'name'=>$restaurant->name,
                    'avg_rating'=>$restaurant->avg_rating,
                    'created_at'=>$restaurant->created_at,
                    'comments'=>CommentResource::collection($restaurant->comments),

                ];
            }),

            'pagination' => $this->pagination
        ];
    }

    public function with($r){
        return [
            'meta' => [
                'message' => 'Api Hit Success',
                'status'=>200,
                'errors'=>[],

            ]

        ];
    }

}
