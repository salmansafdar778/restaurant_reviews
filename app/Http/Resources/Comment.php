<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\User as UserResouce;

class Comment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'id'=>$this->id,
            'restaurant_id'=>$this->restaurant_id,
            'rating'=>$this->rating,
            'comment'=>$this->comment,
            'reply'=>$this->reply,
            'created_at'=>$this->created_at,
            'comment_user'=>$this->comment_user->name ?? '',
            'reply_user'=>$this->reply_user->name ?? '',

        ];

    }

    public function with($r){
        return [
            'meta' => [
                'message' => 'Api Hit Success',
                'status'=>200,
                'errors'=>null,

            ]

        ];
    }
}
