<?php

namespace App\View\Components\Form\Text;

use Illuminate\View\Component;

class Text extends Component
{
    public $label;
    public $type;
    public $id;
    public $name;
    public $value;
    public $placeHolder;
    public $class;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($label=null,$type=null,$id=null,$name=null,$placeHolder=null,$value=null,$class=null)
    {
        $this->label = $label;
        $this->type = $type;
        $this->id = $id;
        $this->name = $name;
        $this->value = $value;
        $this->placeHolder = $placeHolder;
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.text.text');
    }
}
