<?php

namespace App\View\Components\Form\Select;

use Illuminate\View\Component;

class Select extends Component
{
    public $label;
    public $type;
    public $id;
    public $name;
    public $value;
    public $placeHolder;
    public $class;
    public $records;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($label,$type,$id,$name,$value,$placeHolder,$class,$records)
    {
        $this->label = $label;
        $this->type = $type;
        $this->id = $id;
        $this->name = $name;
        $this->value = $value;
        $this->placeHolder = $placeHolder;
        $this->class = $class;
        $this->records = $records;

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.select.select');
    }
}
