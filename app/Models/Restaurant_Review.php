<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurant_Review extends Model
{
    use HasFactory;

    protected $table = 'restaurant_reviews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rating','comment','reply','restaurant_id','comment_user_id','reply_user_id',
    ];

    public function comment_user()
    {
        return $this->belongsTo(User::class,'comment_user_id');
    }

    public function reply_user()
    {
        return $this->belongsTo(User::class,'reply_user_id');
    }

    public function restaurant()
    {
        return $this->belongsTo(Restaurant::class,'restaurant_id');
    }
}
