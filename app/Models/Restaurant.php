<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    use HasFactory;

    protected $table = 'restaurants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','restaurants','user_id',
    ];

    public function comments()
    {
        return $this->hasMany(Restaurant_Review::class);
    }

    public function getAvgRatingAttribute()
    {
        return $this->comments()->average('rating') ?? 0;
    }

    public function getHigRatingAttribute()
    {
        return $this->comments()->max('rating') ?? 0;
    }

    public function getLowRatingAttribute()
    {
        return $this->comments()->min('rating') ?? 0;
    }

    public function latestComment()
    {
        return $this->hasOne(Restaurant_Review::class)->latest();
    }

    public function restaurant_user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
